<?php

namespace Drupal\ixm_dashboard_analytics\Plugin\ixm_dashboard\Display;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\ixm_dashboard\DisplayBase;
use Drupal\ixm_dashboard\DisplayInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a IXM dashboard display/widget for GA information.
 *
 * @IxmDashboardDisplay(
 *   id = "analytics",
 *   label = @Translation("Analytics"),
 *   description = @Translation("Shows google analytics information."),
 *   icon="graphic_eq",
 *   status=TRUE,
 *   widget=TRUE,
 *   settings={
 *    "tray"={
 *      "showPageViews"=TRUE,
 *      "showBounceRate"=TRUE,
 *      "showChannels"=TRUE,
 *      "showSessions"=TRUE,
 *      "showAvgTime"=TRUE,
 *    },
 *    "dashboard"={
 *      "showPageViews"=TRUE,
 *      "showBounceRate"=TRUE,
 *      "showChannels"=TRUE,
 *      "showSessions"=TRUE,
 *      "showAvgTime"=TRUE,
 *    }
 *   },
 * )
 */
class Analytics extends DisplayBase implements ContainerFactoryPluginInterface, DisplayInterface {

  use StringTranslationTrait;

  /**
   * Custom settings for this plugin.
   *
   * @var array
   */
  protected $settings;

  /**
   * The alias of the current page.
   *
   * @var string
   */
  protected $currentAlias;

  /**
   * The start date for reports.
   *
   * @var int
   */
  protected $startDate;

  /**
   * The end date for reports.
   *
   * @var int
   */
  protected $endDate;

  /**
   * Constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AliasManagerInterface $aliasManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->settings = $this->getSettings();

    // Check the current path from the AJAX call.
    $current_path = \Drupal::request()->get('dashboardPage');

    // If this is the homepage, we need a lil magic here.
    $config = \Drupal::config('system.site');
    $front_uri = $config->get('page.front');
    if ('/' . $current_path == $front_uri) {
      $this->currentAlias = '/';
    }
    // Just get the alias.
    else {
      $this->currentAlias = $aliasManager->getAliasByPath('/' . $current_path);
    }

    // Change dates if ajaxed, otherwise default.
    $start = \Drupal::request()->get('startDate');
    $end = \Drupal::request()->get('endDate');

    // @TODO: Put the defaults as settings?.
    $this->startDate = $start ? strtotime($start) : strtotime('8 days ago');
    $this->endDate = $end ? strtotime($end) : strtotime('-1 day');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('path.alias_manager')
    );
  }

  /**
   * Default chart configuration for all charts.
   *
   * @TODO: Use moduleInvokeAll to allow hooks/alters here.
   *
   * @return array
   *   The chart config.
   */
  protected function getChartConfig() {
    $config = [
      'chart' => [
        'renderTo' => [],
        'backgroundColor' => 'transparent',
        'height' => 250,
      ],
      'title' => [
        'text' => '',
      ],
      'legend' => [
        'align' => 'left',
        'verticalAlign' => 'top',
        'itemStyle' => [
          'color' => 'rgba(255,255,255,0.5)',
        ],
        'itemHoverStyle' => [
          'color' => 'rgba(255,255,255,0.6)',
        ],
      ],
      'xAxis' => [
        [
          'lineColor' => 'rgba(255,255,255,0.09)',
        ],
      ],
      'yAxis' => [
        [
          'gridLineColor' => 'rgba(255,255,255,0.09)',
          'title' => [
            'text' => '',
          ],
        ],
      ],
      'exporting' => [
        'enabled' => FALSE,
      ],
      'credits' => [
        'enabled' => FALSE,
      ],
    ];

    return $config;
  }

  /**
   * Line chart config.
   *
   * @param bool $widget
   *   If this is for widget display.
   *
   * @return array
   *   The Chart config.
   */
  protected function lineConfig($widget = FALSE) {
    $config = $this->getChartConfig();

    $config['colors'] = [
      '#EB7828',
      '#99269A',
    ];

    $config['xAxis'][0]['type'] = 'category';
    $config['xAxis'][0]['tickWidth'] = 0;

    if ($widget) {
      $config['legend']['itemStyle']['color'] = '#787D81';
      $config['yAxis'][0]['gridLineColor'] = '#D8DADA';
    }

    return $config;
  }

  /**
   * Pie chart config.
   *
   * @param bool $widget
   *   If this is for widget display.
   *
   * @return array
   *   The Chart config.
   */
  protected function pieConfig($widget = FALSE) {
    $config = $this->getChartConfig();

    $config['chart']['type'] = 'pie';

    // No Text Shadow.
    $config['plotOptions'] = [
      'pie' => [
        'dataLabels' => [
          'style' => [
            'textShadow' => FALSE,
            'color' => 'rgba(255,255,255,0.5)',
          ],
        ],
      ],
    ];

    // Colors.
    $config['colors'] = [
      '#eb5828',
      '#eb7828',
      '#f5a623',
      '#f2ce1e',
      '#6aa83e',
    ];

    if ($widget) {
      $config['plotOptions']['pie']['dataLabels']['style']['color'] = '#787D81';
    }

    return $config;
  }

  /**
   * Bar chart config.
   *
   * @param bool $widget
   *   If this is for widget display.
   *
   * @return array
   *   The Chart config.
   */
  protected function barConfig($widget = FALSE) {
    $config = $this->getChartConfig();

    $config['chart']['type'] = 'column';

    $config['plotOptions'] = [
      'column' => [
        'borderWidth' => 0,
      ],
    ];

    $config['colors'] = [
      '#EB7828',
      '#99269A',
    ];

    $config['xAxis'][0]['type'] = 'category';
    $config['xAxis'][0]['tickWidth'] = 0;

    return $config;
  }

  /**
   * Build out the page views widget.
   *
   * @return mixed
   *   A renderable array or false.
   */
  protected function buildPageViews($widget = FALSE) {

    // Not on.
    if (!$widget && empty($this->settings['tray']['showPageViews'])) {
      return FALSE;
    }

    // Not on.
    if ($widget && empty($this->settings['dashboard']['showPageViews'])) {
      return FALSE;
    }

    $params = [
      'metrics' => [
        'ga:pageviews',
        'ga:uniquePageviews',
      ],
      'dimensions' => 'ga:date',
      'sort_metric' => 'ga:date',
      'start_date' => $this->startDate,
      'end_date' => $this->endDate,
    ];

    if (!$widget) {
      $params['filters'] = 'ga:pagePath==' . $this->currentAlias;
    }

    $daily = google_analytics_reports_api_report_data($params);

    if (!empty($daily->results->rows)) {
      $block = [];
      $total = $daily->results->totalsForAllResults['pageviews'];
      $results = $daily->results->rows;

      $block['header'] = [
        '#markup' => '
          <div>
            <div class="title">' . $this->t('Page Views') . '</div>
            <div class="stat">' . $total . '</div>
          </div>',
      ];

      $block['chart'] = [
        '#type' => 'container',
        '#attributes' => [
          'data-chart-id' => 'pageViews',
          'class' => ['ixd-analytic-chart'],
        ],
      ];

      $csv = [0 => [NULL, "Recurrent", "Unique"]];
      foreach ($results as $id => $row) {
        $csv[] = [
          date('M j', $row['date']),
          $row['pageviews'],
          $row['uniquePageviews'],
        ];
      }
      $block['#attached']['drupalSettings']['ixd_analytics'] = [
        'pageViews' => [
          'config' => JSON::encode($this->lineConfig($widget)),
          'csv' => JSON::encode($csv),
        ],
      ];

      return $block;
    }

    return FALSE;
  }

  /**
   * Build out the bouncerate widget.
   *
   * @return mixed
   *   A renderable array or false.
   */
  protected function buildBounceRate($widget = FALSE) {

    // Not on.
    if (!$widget && empty($this->settings['tray']['showBounceRate'])) {
      return FALSE;
    }

    // Not on.
    if ($widget && empty($this->settings['dashboard']['showBounceRate'])) {
      return FALSE;
    }

    $params = [
      'metrics' => 'ga:bounceRate',
      'dimensions' => 'ga:date',
      'sort_metric' => 'ga:date',
      'start_date' => $this->startDate,
      'end_date' => $this->endDate,
    ];

    if (!$widget) {
      $params['filters'] = 'ga:pagePath==' . $this->currentAlias;
    }

    $daily = google_analytics_reports_api_report_data($params);

    if (!empty($daily->results->rows)) {
      $block = [];
      $total = $daily->results->totalsForAllResults['bounceRate'];
      $results = $daily->results->rows;

      $block['header'] = [
        '#markup' => '
          <div>
            <div class="title">' . $this->t('Bounce Rate') . '</div>
            <div class="stat">' . number_format($total, 2) . '%</div>
          </div>',
      ];

      $block['chart'] = [
        '#type' => 'container',
        '#attributes' => [
          'data-chart-id' => 'bounceRate',
          'class' => ['ixd-analytic-chart'],
        ],
      ];

      $csv = [0 => [NULL, "Rate"]];
      foreach ($results as $id => $row) {
        $csv[] = [
          date('M j', $row['date']),
          number_format($row['bounceRate'], 2),
        ];
      }

      $line_config = $this->lineConfig($widget);
      $line_config['legend']['enabled'] = FALSE;
      $block['#attached']['drupalSettings']['ixd_analytics'] = [
        'bounceRate' => [
          'config' => JSON::encode($line_config),
          'csv' => JSON::encode($csv),
        ],
      ];

      return $block;
    }

    return FALSE;
  }

  /**
   * Build out the channels widget.
   *
   * @return mixed
   *   A renderable array or false.
   */
  protected function buildChannels($widget = FALSE) {

    // Not on.
    if (!$widget && empty($this->settings['tray']['showChannels'])) {
      return FALSE;
    }

    // Not on.
    if ($widget && empty($this->settings['dashboard']['showChannels'])) {
      return FALSE;
    }

    $params = [
      'metrics' => 'ga:users',
      'dimensions' => 'ga:channelGrouping',
      'start_date' => $this->startDate,
      'end_date' => $this->endDate,
    ];

    if (!$widget) {
      $params['filters'] = 'ga:pagePath==' . $this->currentAlias;
    }

    $totals = google_analytics_reports_api_report_data($params);

    if (!empty($totals->results->rows)) {
      $block = [];

      $block['header'] = [
        '#markup' => '<div class="title">' . $this->t('Channels') . '</div>',
      ];

      $block['chart'] = [
        '#type' => 'container',
        '#attributes' => [
          'data-chart-id' => 'channels',
          'class' => ['ixd-analytic-chart'],
        ],
      ];

      $config = $this->pieConfig($widget);

      $csv = [0 => [NULL, "Users"]];
      foreach ($totals->results->rows as $id => $row) {
        $csv[] = [$row['channelGrouping'], $row['users']];
      }

      $block['#attached']['drupalSettings']['ixd_analytics'] = [
        'channels' => [
          'config' => JSON::encode($config),
          'csv' => JSON::encode($csv),
        ],
      ];

      return $block;

    }
    return FALSE;
  }

  /**
   * Build out the sessions widget.
   *
   * @return mixed
   *   A renderable array or false.
   */
  protected function buildSessions($widget = FALSE) {

    // Not on.
    if (!$widget && empty($this->settings['tray']['showSessions'])) {
      return FALSE;
    }

    // Not on.
    if ($widget && empty($this->settings['dashboard']['showSessions'])) {
      return FALSE;
    }

    $params = [
      'metrics' => 'ga:sessions',
      'dimensions' => 'ga:date',
      'start_date' => $this->startDate,
      'end_date' => $this->endDate,
      'sort_metric' => 'ga:date',
    ];

    if (!$widget) {
      $params['filters'] = 'ga:pagePath==' . $this->currentAlias;
    }

    $totals = google_analytics_reports_api_report_data($params);

    if (!empty($totals->results->rows)) {
      $block = [];

      $session_count = 0;
      $csv = [0 => [NULL, "Sessions"]];
      foreach ($totals->results->rows as $id => $row) {
        $csv[] = [date('M j', $row['date']), $row['sessions']];
        $session_count += $row['sessions'];
      }

      $block['container'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['row', 'no-gutters'],
        ],
      ];

      // Wrap in widget box on dashboard page.
      if ($widget) {
        $block['container']['#prefix'] = '<div class="widget-content widget-box">';
        $block['container']['#suffix'] = '</div>';
      }

      $block['container']['chart'] = [
        '#type' => 'container',
        '#attributes' => [
          'data-chart-id' => 'sessions',
          'class' => ['col-md-6 ixd-analytic-chart'],
        ],
      ];

      $block['container']['info'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['col-md-6'],
        ],
        'markup' => [
          '#markup' => '
            <div class="title">' . $this->t("Sessions") . '</div>
            <div class="stat">' . $session_count . '</div>
            <p>' . $this->t("Unique sessions in the selected date range.") . '</p>
          ',
        ],
      ];

      // Modify chart settings.
      $line_config = $this->lineConfig();
      $line_config['chart']['height'] = '180';
      $line_config['legend']['enabled'] = FALSE;
      $line_config['xAxis'][0]['labels']['enabled'] = FALSE;
      $line_config['yAxis'][0]['labels']['enabled'] = FALSE;

      $block['#attached']['drupalSettings']['ixd_analytics'] = [
        'sessions' => [
          'config' => JSON::encode($line_config),
          'csv' => JSON::encode($csv),
        ],
      ];

      return $block;

    }
    return FALSE;
  }

  /**
   * Build out the avg time widget.
   *
   * @return mixed
   *   A renderable array or false.
   */
  protected function buildAvgTime($widget = FALSE) {

    // Not on.
    if (!$widget && empty($this->settings['tray']['showAvgTime'])) {
      return FALSE;
    }

    // Not on.
    if ($widget && empty($this->settings['dashboard']['showAvgTime'])) {
      return FALSE;
    }

    $params = [
      'metrics' => 'ga:avgTimeOnPage',
      'start_date' => $this->startDate,
      'end_date' => $this->endDate,
      'dimensions' => 'ga:date',
      'sort_metric' => 'ga:date',
    ];

    if (!$widget) {
      $params['filters'] = 'ga:pagePath==' . $this->currentAlias;
    }

    $daily = google_analytics_reports_api_report_data($params);

    if (!empty($daily->results->rows)) {
      $page_total = $daily->results->totalsForAllResults['avgTimeOnPage'];

      $block = [];

      $csv = [0 => [NULL, "avg"]];
      foreach ($daily->results->rows as $id => $row) {
        $csv[] = [
          date('M j', $row['date']),
          number_format($row['avgTimeOnPage'], 2),
        ];
      }

      $block['container'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['row', 'no-gutters'],
        ],
      ];

      // Wrap in widget-box if on dashboard page.
      if ($widget) {
        $block['container']['#prefix'] = '<div class="widget-box">';
        $block['container']['#suffix'] = '</div>';
      }

      $block['container']['chart'] = [
        '#type' => 'container',
        '#attributes' => [
          'data-chart-id' => 'avgTimeOnPage',
          'class' => ['col-md-6 ixd-analytic-chart'],
        ],
      ];

      $block['container']['info'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['col-md-6'],
        ],
        'markup' => [
          '#markup' => '
            <div class="title">' . $this->t("Avg Time on Page") . '</div>
            <div class="stat">' . gmdate('H:i:s', $page_total) . '</div>
          ',
        ],
      ];

      // Add in the site-wide average.
      if (!$widget) {
        unset($params['filters']);

        $totals = google_analytics_reports_api_report_data($params);
        if (!empty($totals->results->rows)) {
          $site_total = $totals->results->totalsForAllResults['avgTimeOnPage'];
          $block['container']['info']['markup']['#markup'] .= '<p>' . $this->t("Site Avg: @avg", ['@avg' => gmdate('H:i:s', $site_total)]) . '</p>';
        }
      }

      // Modify chart settings.
      $line_config = $this->barConfig();
      $line_config['chart']['height'] = '180';
      $line_config['legend']['enabled'] = FALSE;
      $line_config['xAxis'][0]['labels']['enabled'] = FALSE;
      $line_config['yAxis'][0]['labels']['enabled'] = FALSE;

      $block['#attached']['drupalSettings']['ixd_analytics'] = [
        'avgTimeOnPage' => [
          'config' => JSON::encode($line_config),
          'csv' => JSON::encode($csv),
        ],
      ];

      return $block;

    }
    return FALSE;
  }

  /**
   * Simple date picker array.
   *
   * @return array
   *   a render array.
   */
  protected function buildDatepicker($widget = FALSE) {
    $widget_class = $widget ? 'use-query' : '';

    return [
      '#markup' => '
        <div class="analytics-date-selector text-align-right">
          <a href="#" class="date-selector clearfix">
            <span class="start">' . date('j M Y', $this->startDate) . '</span> - 
            <span class="end">' . date('j M Y', $this->endDate) . '</span>
            <i class="material-icons">keyboard_arrow_down</i>
          </a>
          <div id="analytics-date" class="float-right my-2 ' . $widget_class . '"></div>
        </div>
      ',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    if ($page_views = $this->buildPageViews()) {
      $output['row1']['page-views'] = $page_views;
    }

    if ($channels = $this->buildChannels()) {
      $output['row2']['channels'] = $channels;
    }

    if ($page_views = $this->buildBounceRate()) {
      $output['row3']['bounce-rate'] = $page_views;
    }

    if ($sessions = $this->buildSessions()) {
      $output['row4']['sessions'] = $sessions;
    }

    if ($avg_time = $this->buildAvgTime()) {
      $output['row4']['avg_time'] = $avg_time;
    }

    // No data.
    if (empty($output)) {
      $output = [
        '#markup' => $this->t('There is currently no analytic data for this page.'),
      ];
    }
    else {
      $datepicker['datepicker'] = $this->buildDatepicker();
      $output = array_merge($datepicker, $output);
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function buildWidget() {

    if ($page_views = $this->buildBounceRate(TRUE)) {
      $output['row1']['bounce-rate'] = $page_views;
    }
    if ($sessions = $this->buildSessions(TRUE)) {
      $output['row1']['group']['sessions'] = $sessions;
    }
    if ($avg_time = $this->buildAvgTime(TRUE)) {
      $output['row1']['group']['avg_time'] = $avg_time;
    }

    // Don't widget box this group.
    if (isset($output['row1']['group'])) {
      $output['row1']['group']['#no_widget'] = TRUE;
    }

    if ($page_views = $this->buildPageViews(TRUE)) {
      $output['row2']['page-views'] = $page_views;
    }

    if ($channels = $this->buildChannels(TRUE)) {
      $output['row2']['channels'] = $channels;
    }

    // No data.
    if (empty($output)) {
      $output = [
        '#markup' => $this->t('There is currently no analytic data for this page.'),
      ];
    }
    else {
      $datepicker['datepicker'] = $this->buildDatepicker(TRUE);
      $output = array_merge($datepicker, $output);
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    // @TODO: Once widgets are on their own, this will be just for the tray.
    $form['tray'] = [
      '#title' => $this->t('Analytics Display Widgets'),
      '#type' => 'details',
      '#open' => TRUE,
    ];

    $tray = isset($this->settings['tray']) ? $this->settings['tray'] : [];
    $form['tray']['showPageViews'] = [
      '#title' => $this->t('Show Page Views'),
      '#type' => 'checkbox',
      '#default_value' => isset($tray['showPageViews']) ? $tray['showPageViews'] : NULL,
    ];

    $form['tray']['showChannels'] = [
      '#title' => $this->t('Show Channels'),
      '#type' => 'checkbox',
      '#default_value' => isset($tray['showChannels']) ? $tray['showChannels'] : NULL,
    ];

    $form['tray']['showBounceRate'] = [
      '#title' => $this->t('Show Bounce Rate'),
      '#type' => 'checkbox',
      '#default_value' => isset($tray['showBounceRate']) ? $tray['showBounceRate'] : NULL,
    ];

    $form['tray']['showSessions'] = [
      '#title' => $this->t('Show Sessions'),
      '#type' => 'checkbox',
      '#default_value' => isset($tray['showSessions']) ? $tray['showSessions'] : NULL,
    ];

    $form['tray']['showAvgTime'] = [
      '#title' => $this->t('Show Average Time on Page'),
      '#type' => 'checkbox',
      '#default_value' => isset($tray['showAvgTime']) ? $tray['showAvgTime'] : NULL,
    ];

    // Dashboard widgets.
    $form['dashboard'] = [
      '#title' => $this->t('Dashboard Widgets'),
      '#type' => 'details',
      '#open' => TRUE,
    ];

    $dashboard = isset($this->settings['dashboard']) ? $this->settings['dashboard'] : [];
    $form['dashboard']['showPageViews'] = [
      '#title' => $this->t('Show Page Views'),
      '#type' => 'checkbox',
      '#default_value' => isset($dashboard['showPageViews']) ? $dashboard['showPageViews'] : NULL,
    ];

    $form['dashboard']['showChannels'] = [
      '#title' => $this->t('Show Channels'),
      '#type' => 'checkbox',
      '#default_value' => isset($dashboard['showChannels']) ? $dashboard['showChannels'] : NULL,
    ];

    $form['dashboard']['showBounceRate'] = [
      '#title' => $this->t('Show Bounce Rate'),
      '#type' => 'checkbox',
      '#default_value' => isset($dashboard['showBounceRate']) ? $dashboard['showBounceRate'] : NULL,
    ];

    $form['dashboard']['showSessions'] = [
      '#title' => $this->t('Show Sessions'),
      '#type' => 'checkbox',
      '#default_value' => isset($dashboard['showSessions']) ? $dashboard['showSessions'] : NULL,
    ];

    $form['dashboard']['showAvgTime'] = [
      '#title' => $this->t('Show Average Time on Page'),
      '#type' => 'checkbox',
      '#default_value' => isset($dashboard['showAvgTime']) ? $dashboard['showAvgTime'] : NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries() {
    return [
      'easychart/easychart.render',
      'easychart/lib.highcharts',
      'easychart/lib.easycharts.render',
      'ixm_dashboard_analytics/ixd_analytics',
    ];
  }

}
