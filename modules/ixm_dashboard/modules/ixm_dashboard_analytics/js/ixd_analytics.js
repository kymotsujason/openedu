(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.ixdAnalytics = {
    attach: function attach(context, settings) {

      var $charts = $('.ixd-analytic-chart', context);
      if ($charts.length) {

        $charts.each(function () {
          var id = $(this).attr('data-chart-id');

          // Ensure we have chart data.
          if (settings.ixd_analytics[id] !== undefined) {
            var chartSettings = settings.ixd_analytics[id];
            window.easychart = new ec({element: this});

            // Add config
            var config = JSON.parse(chartSettings.config);
            window.easychart.setConfig(config);

            // Add data
            var csv = JSON.parse(chartSettings.csv);
            window.easychart.setData(csv);
          }
        });
      }


      // Datepicker
      var $dateSelector = $('.date-selector', context);
      if ($dateSelector.length) {
        var $picker = $('#analytics-date', context);

        $picker.hide().datepicker({
          'dateFormat': 'yyyy-mm-dd',
          'range': true,
          'inline': true,
          'language': 'en',
          'position': 'bottom right',
          'onSelect': function (inst, animationCompleted) {
            var components = inst.split(',');
            var from = (0 in components) ? components[0] : false;
            var to = (1 in components) ? components[1] : false;

            if (to && from) {

              if ($picker.hasClass('use-query')) {
                window.location.search += '&startDate=' + from + '&endDate=' + to;
              }
              else {
                var ajaxer = false;
                $.each(Drupal.ajax.instances, function (idx, item) {
                  if (item.selector === '#display-analytics') {
                    ajaxer = item;
                    return false;
                  }
                });

                if (ajaxer) {
                  ajaxer.submit.startDate = from;
                  ajaxer.submit.endDate = to;

                  // Clear the charts.
                  var $dashboardTray = $('#ixm-dashboard-tray');
                  $dashboardTray.find('.tray-content').html("");
                  $dashboardTray.addClass('loading');

                  $('#display-analytics').trigger('ixm-ajax-display');
                }
              }
            }
          }
        });

        // Show/hide.
        $dateSelector.click(function () {
          $picker.toggle();
          return false;
        });
      }

    }
  };

})(jQuery, Drupal, drupalSettings);
